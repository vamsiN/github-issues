
const Pagenation = (props) => {
    const { data, pageHandler } = props
    let pageNumbers = []
    for (let i = 1; i < Math.ceil(data.length / 30) + 1; i++) {
        pageNumbers.push(i)
    }
    return (
        <div>
            <center className="page-button-container">
                {pageNumbers.map(page => <div className="pageButton" key={page} onClick={() => { pageHandler(page) }}>{page}</div>)}
            </center>
        </div>
    )
}

export default Pagenation