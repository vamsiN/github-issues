// import {Link} from 'react-router-dom'
import { Component } from 'react'
import { CgRadioChecked } from 'react-icons/cg'
import { FaRegCommentAlt } from 'react-icons/fa'
import Axios from 'axios'
import './Issue.css'



class Issue extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title: props.issuedetails.title,
            number: props.issuedetails.number,
            comments: props.issuedetails.comments,
            login: props.issuedetails.user.login,
            labelArray: []
        }

    }


    // adding labels

    getLabels() {
        const { number } = this.state
        let url = `https://api.github.com/repos/facebook/create-react-app/issues/${number}/labels`
        Axios.get(url).then((response) => {
            console.log(response)


        })
    }

    componentDidMount() {
        this.getLabels()
    }

    render() {

        const { title, number, comments, login } = this.state
        return (
            // adding routing logic
            <>
                <li className='issue-list-item'>

                    <div>
                        < CgRadioChecked className="circular-icon" />
                    </div>
                    <div className='issue-details-container'>
                        <h1 className="issue-title">{title}</h1>
                        <div className='issue-info'>
                            <span>` #{number} opened by {login}`</span>
                        </div>
                    </div>
                    <div className='icons-container'>
                        <div className='comment-icon-container'>
                            < FaRegCommentAlt className='comment-icon' /><span>{comments}</span>
                        </div>
                    </div>
                </li>
            </>

        )

    }



}

export default Issue