import { Component } from 'react'
import axios from 'axios'
import Issue from '../Issue/Issue'
import Error from '../Error/Error'
import Loader from '../Loader/Loader'
import { BsSearch } from 'react-icons/bs'


// import Pagenation from '../Pagenation/Pagenation'

import './IssuePage.css'
class IssuePage extends Component {

    APP_STATE = {
        LOADING: 'loading',
        LOADED: 'loaded',
        ERROR: 'error',
    }

    state = {
        githubIssuesArray: [],
        searchInput: "",
        perPageArray: [],
        status: this.APP_STATE.LOADING,
    }


    getIssues = () => {
        axios.get('https://api.github.com/repos/facebook/create-react-app/issues?state=open&per_page=30')  //issues?per_page=25&page=2
            .then(
                (response) => {
                    console.log(this.state.status)
                    this.setState({
                        githubIssuesArray: response.data,
                        status: this.APP_STATE.LOADED,
                    });
                    console.log(this.state.status)
                }
            ).catch((error) => {
                this.setState({
                    status: this.APP_STATE.ERROR
                })
                console.log(error)
            })

    }

    onChangeSearchInput = event => { this.setState({ searchInput: event.target.value }) }

    componentDidMount() {
        this.getIssues();
    }

    pageHandler = (pageNumber) => {
        const { githubIssuesArray } = this.state
        this.setState({ perPageArray: githubIssuesArray.slice((pageNumber * 30) - 30, pageNumber * 30) })
    }


    render() {

        const { githubIssuesArray, searchInput, status } = this.state
        console.log(status)
        const searchResults = githubIssuesArray.filter(eachIssue => eachIssue.title.toLowerCase().includes(searchInput.toLowerCase()))


        return (
            <div className='bg-container'>
                <div className='header'>
                    <span className="package-owner">facebook/</span><span className="pakage-name">create-react-app</span>
                    <h1 className="issues-heading">Open Issues</h1>
                </div>
                <div className="search-input-container">
                    < BsSearch alt="search icon" className="search-icon" />
                    <input type='search' className='search-input' placeholder='Search Issues' onChange={this.onChangeSearchInput} value={searchInput} />
                </div>
                <ul className="issues-container">
                    {this.state.status === this.APP_STATE.LOADING ?
                        <>
                            <Loader />
                        </>
                        :
                        undefined
                    }
                    {this.state.status === this.APP_STATE.ERROR ?
                        <>
                            < Error />
                        </>
                        :
                        undefined
                    }
                    {this.state.status === this.APP_STATE.LOADED ?
                        <>
                            {this.state.githubIssuesArray.lenth === 0 ?
                                <>
                                    No Products Available
                                </>
                                :
                                searchResults.map((each) => (
                                    < Issue issuedetails={each} key={each.id} />
                                ))

                            }
                        </>
                        :
                        undefined
                    }
                </ul>
                <br />
                {/* <Pagenation data={githubIssuesArray} pageHandler={this.pageHandler} /> */}
            </div>
        )
    }

}

export default IssuePage;